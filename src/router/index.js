import React from 'react';
import { Routes, Route } from 'react-router-dom';
import HomePage from '../views/pages/HomePage';
import HistoryOrderView from '../views/order/HistoryOrderView';
import FoodBillView from '../views/order/FoodBillView';
import MenuListView from '../views/order/MenuListView';
import NotFoundPage from '../views/pages/NotFoundPage';

const RouterConfig = () => (
    <Routes>
        <Route 
            path="/" 
            name="home"
            element={<HomePage />} 
        />
        <Route 
            path="/menu-list" 
            name="menu-list"
            element={<MenuListView />} 
        />
        <Route 
            path="/history-order" 
            name="history-order"
            element={<HistoryOrderView />} 
        />
        <Route 
            path="/food-bill" 
            name="food-bill"
            element={<FoodBillView />} 
        />
        <Route 
            path="/404" 
            name="404"
            element={<NotFoundPage />} 
        />
    </Routes>
);

export default RouterConfig;

// import HomePage from '../views/pages/HomePage';
// import NotFoundPage from '../views/pages/NotFoundPage';

// const routes = [
//     {
//         path: '/',
//         element: <HomePage />,
//         name: 'home',
//         meta: {
//             // layout: "LayoutMain"
//             layout: "LayoutFull"
//         }
//     },
//     {
//         path: '/404',
//         element: <NotFoundPage />,
//         name: '404',
//         meta: {
//             layout: "LayoutFull"
//         }
//     }
// ];

// export default routes;
