import React, { useContext } from 'react';
import { Routes, Route } from 'react-router-dom';
import MetaContext from './metaContext';
import routeConfig from './index';

const RouterConfig = () => {
    const { setMeta } = useContext(MetaContext);

    return (
        <Routes>
            {routeConfig.map((route) => (
                <Route 
                    key={route.path}
                    path={route.path}
                    element={
                        <>
                            {setMeta(route.meta)}
                            {route.element}
                        </>
                    }
                />
            ))}
        </Routes>
    );
};

export default RouterConfig;
