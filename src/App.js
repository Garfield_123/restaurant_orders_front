import React from 'react';
import LeyoutFull from './layout/full';

function App() {
  
  return (
    <>
      <LeyoutFull />
    </>
  );
}

export default App;

// ******************************************************
// import React, { useState, useMemo } from 'react';
// import RouterConfig from './router/routerConfig';
// import MetaContext from './router/metaContext';
// // import { useHistory } from "react-router-dom";

// function App() {
//   // const history = useHistory()
//   // console.log('eeeeee----',history)
//     const [meta, setMeta] = useState({});

//     // Ensure the setMeta doesn't change on every render
//     const contextValue = useMemo(() => ({
//         meta,
//         setMeta: (newMeta) => setMeta(newMeta)
//     }), [meta]);

//     console.log("Current meta data:", meta.layout);

//     return (
//         <MetaContext.Provider value={contextValue}>
//           {/* <Component is={layout}/> */}
//             <RouterConfig />
//         </MetaContext.Provider>
//     );
// }

// export default App;

// ******************************************************

// import React, { useState, useMemo } from 'react';
// // import RouterConfig from './router/routerConfig';
// import MetaContext from './router/metaContext';
// import './styles/main.scss';

// // import LeyoutMain from './layout/main';
// import LeyoutFull from './layout/full';

// function App() {
//     const [meta, setMeta] = useState({});

//     const contextValue = useMemo(() => ({
//         meta,
//         setMeta: (newMeta) => setMeta(newMeta)
//     }), [meta]);

//     // console.log("Current meta data:", meta.layout);

//     return (
//         <MetaContext.Provider value={contextValue}>
//           {/* { meta.layout === 'LayoutMain' ? <LeyoutMain/> : <LeyoutFull/>} */}
//           {/* <LeyoutMain/> */}
//           <LeyoutFull/>
//         </MetaContext.Provider>
//     );
// }

// export default App;