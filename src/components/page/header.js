import React from "react";
import { CloseOutlined } from '@ant-design/icons';
import { useNavigate } from "react-router-dom";

const Header = ({children}) => {
    const navigate = useNavigate();

    return (
        <header className="topbar">
            <div style={{flexFlow:'nowrap'}}>
                <div style={{height:'4rem', display:'flex',justifyContent: 'space-between',alignItems: 'center'}}>
                    { children }
                    <div/>
                </div>
            </div>
        </header>
    )
}

export default Header;