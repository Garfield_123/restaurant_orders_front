import React from "react";

const Footer = ({children}) => {

    return (
        <footer className="footer" style={{display:'flex', alignItems:'center'}}>
            <div style={{flexFlow:'nowrap',width:'100%'}}>
                { children }
            </div>
        </footer>
    )
}

export default Footer;