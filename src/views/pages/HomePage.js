import React, { useState } from "react";
import { RightCircleFilled, FileTextOutlined, UserOutlined, QrcodeOutlined, FlagTwoTone } from '@ant-design/icons';
import { Avatar, FloatButton, Button, Card, Badge, Modal, Tag, QRCode } from 'antd';
import { useNavigate } from "react-router-dom";

const HomePage = () => {
    const navigate = useNavigate();
    const [isOpenCall, setIsOpenCall] = useState(false)
    const [isOpenQr, setIsOpenQr] = useState(false)

    return (
        <>
            <div>
                <img src={'../images/logo.jpg'} style={{width: '100%', opacity: '0.5'}}/>
            </div>
            <div
                style={{
                    background: 'rgb(255 255 255 / 0%)',
                    position: 'relative',
                    bottom: '50px'
                }}
            >
                <FloatButton 
                    style={{
                        top: '20px',
                    }}
                    icon={<FlagTwoTone />}
                />
                <div style={{textAlign: 'center'}}>
                    <Avatar
                        size={{ xs: 100, md: 140, lg: 160, xl: 180 }}
                        src={'../images/logo.jpg'}
                    />  
                    <div style={{marginTop: '20px'}}>
                        <div>ร้าน ฟฟฟฟ กกกกกกก</div>
                        <div style={{margin: '5px 0'}}>location</div>
                        <div>โต๊ะ: <b>99</b> | คุณ: <b>A</b> </div>
                    </div>
                    <div style={{margin: '10px 0', display: 'flex', justifyContent: 'center'}}>
                        <Button 
                            type="primary" 
                            size='large' 
                            style={{ width: '80%', display: 'flex', justifyContent: 'space-between' }}
                            onClick={() => navigate("/menu-list")}
                        >
                            <span>สั่งอาหาร</span>
                            <span><RightCircleFilled /></span>
                        </Button>
                    </div>
                    <div style={{display: 'flex', justifyContent: 'center', marginTop: '20px'}}>
                        <Card
                            style={{
                                width: 300,
                                margin: '0 5px',
                                boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                            }}
                        >
                            <div style={{cursor: 'pointer'}} onClick={() => navigate("/history-order")}>
                                <div className='icon-order-list'>
                                    <Badge count={7}>
                                        <FileTextOutlined />
                                    </Badge>
                                </div>
                                <div>รายการที่สั่ง</div>
                            </div>
                        </Card>
                        <Card
                            style={{
                                width: 300,
                                margin: '0 5px',
                                boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                            }}
                        >
                            <div style={{cursor: 'pointer'}} onClick={() => setIsOpenCall(true)}>
                                <div className='icon-order-list'>
                                    <UserOutlined />
                                </div>
                                <div>เรียกพนักงาน</div>
                            </div>
                        </Card>
                        <Card
                            style={{
                                width: 300,
                                margin: '0 5px',
                                boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                            }}
                        >
                            <div style={{cursor: 'pointer'}} onClick={() => setIsOpenQr(true)}>
                                <div className='icon-order-list'>
                                    <QrcodeOutlined />
                                </div>
                                <div>แชร์ QR ให้เพื่อน</div>
                            </div>
                        </Card>
                    </div>
                </div>
            </div>

            <Modal
                centered
                open={isOpenCall}
                onCancel={() => setIsOpenCall(false)}
                footer={null}
            >
                <div style={{textAlign:'center'}}>
                    <b>เรียกพนักงาน</b> 
                    <br/> 
                    ต้องการให้พนักงานช่วยเหลือทางด้านใด?
                </div>
                <div style={{display:'flex', justifyContent:'center',marginTop:'20px',textAlign:'center'}}>
                    <Card
                        style={{
                            width: 300,
                            margin: '0 5px',
                            boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                        }}
                    >
                        <div style={{cursor: 'pointer'}}>
                            <div className='icon-order-list'>
                                <UserOutlined />
                            </div>
                            <div>ขออุปกรณ์</div>
                        </div>
                    </Card>
                    <Card
                        style={{
                            width: 300,
                            margin: '0 5px',
                            boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                        }}
                    >
                        <div style={{cursor: 'pointer'}}>
                            <div className='icon-order-list'>
                                <UserOutlined />
                            </div>
                            <div>ขอเครื่องปรุง</div>
                        </div>
                    </Card>
                </div>
                <div style={{display:'flex', justifyContent:'center',marginTop:'20px',textAlign:'center'}}>
                    <Card
                        style={{
                            width: 300,
                            margin: '0 5px',
                            boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                        }}
                    >
                        <div style={{cursor: 'pointer'}}>
                            <div className='icon-order-list'>
                                <UserOutlined />
                            </div>
                            <div>เติมเครื่องดื่ม</div>
                        </div>
                    </Card>
                    <Card
                        style={{
                            width: 300,
                            margin: '0 5px',
                            boxShadow: '0 1px 2px -2px rgb(0 0 0 / 16%), 0 3px 6px 0 rgb(0 0 0 / 12%), 0 5px 12px 4px rgb(0 0 0 / 9%)'
                        }}
                    >
                        <div style={{cursor: 'pointer'}}>
                            <div className='icon-order-list'>
                                <UserOutlined />
                            </div>
                            <div>อื่นๆ</div>
                        </div>
                    </Card>
                </div>
            </Modal>

            <Modal
                centered
                open={isOpenQr}
                onCancel={() => setIsOpenQr(false)}
                footer={null}
            >
                <div style={{textAlign:'center'}}>
                    <div>
                        <b>สั่งอาหารกับเพื่อน</b>
                        <br/>
                        เพียงให้เพื่อนสแกน QR Code
                    </div>
                    <div style={{margin:'20px 0'}}>
                        <Tag color="blue">โต๊ะ 15</Tag>
                    </div>
                    <div style={{display:'flex',justifyContent:'center'}}>
                        <QRCode value={'http://localhost:3000/' || '-'} />
                    </div>
                </div>
            </Modal>
        </>
    );
}

export default HomePage;