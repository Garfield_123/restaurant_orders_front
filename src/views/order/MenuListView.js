import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Avatar, Badge, Tabs, Drawer, Col, Row } from 'antd';
import { 
    LeftOutlined, 
    FileTextOutlined, 
    SearchOutlined, 
    UnorderedListOutlined,
    CheckOutlined,
    DownOutlined 
} from '@ant-design/icons';
import FooterPage from "../../components/page/footer";
const { TabPane } = Tabs;

const MenuListView = () => {
    const navigate = useNavigate();

    const [open, setOpen] = useState(false);

    const drawerHeader = (
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <div>หมวดหมู่ทั้งหมด</div>
          <Button type="text" icon={<DownOutlined />} onClick={() => onClose} />
        </div>
    );

    const contentTab = (
        <>
            <div style={{marginBottom:'15px'}}>
                เมนูแนะนำ
            </div>
            <div>
                <div style={{display:'flex'}}>
                    <div>
                        <img 
                            src={'../../images/logo.jpg'} 
                            style={{width: '100px', height: '90px',borderRadius: '12px',objectFit: 'cover'}}
                        />
                    </div>
                    <div style={{paddingLeft:'15px'}}>
                        <div> เฉาก๊วยชากังลาว 1 </div>
                        <div> ฿25.00 </div>
                    </div>
                </div>
                <hr/>
            </div>
            <div>
                <div style={{display:'flex'}}>
                    <div>
                        <img 
                            src={'../../images/logo.jpg'} 
                            style={{width: '100px', height: '90px',borderRadius: '12px',objectFit: 'cover'}}
                        />
                    </div>
                    <div style={{paddingLeft:'15px'}}>
                        <div> เฉาก๊วยชากังลาว 2 </div>
                        <div> ฿35.00 </div>
                    </div>
                </div>
                <hr/>
            </div>
            <div>
                <div style={{display:'flex'}}>
                    <div>
                        <img 
                            src={'../../images/logo.jpg'} 
                            style={{width: '100px', height: '90px',borderRadius: '12px',objectFit: 'cover'}}
                        />
                    </div>
                    <div style={{paddingLeft:'15px'}}>
                        <div> เฉาก๊วยชากังลาว 3 </div>
                        <div> ฿45.00 </div>
                    </div>
                </div>
                <hr/>
            </div>
        </>
    );

    const items = [
        {
          key: '2',
          label: 'เมนูแนะนำ',
          children: contentTab,
        },
        {
          key: '3',
          label: 'กะเพรา',
          children: 'Content of Tab Pane 3',
        },
        {
          key: '4',
          label: 'ต้มยำ',
          children: 'Content of Tab Pane 4',
        },
        {
          key: '5',
          label: 'เมนูเส้น',
          children: 'Content of Tab Pane 5',
        },
        {
          key: '6',
          label: 'จำนวนลูกค้า',
          children: 'Content of Tab Pane 6',
        }
    ];

    const onChange = (key) => {
        console.log(key);
        if(key === 1)
            setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };

    return (
        <div>
            <header className="topbar">
                <div style={{flexFlow:'nowrap'}}>
                    <div style={{height:'4rem', display:'flex',alignItems:'center',justifyContent:'space-between'}}>
                        <div style={{display:'flex',alignItems:'center'}}>
                            <div style={{margin: '0 20px'}}>
                                <LeftOutlined onClick={() => navigate("/history-order")}/>
                            </div>
                            <div>
                                <Avatar
                                    size={{ xs: 30, md: 60, lg: 120, xl: 180 }}
                                    src={'../../images/logo.jpg'}
                                /> 
                            </div>
                        </div>
                        <div style={{display:'flex',alignItems:'center',paddingRight:'20px'}}>
                            <SearchOutlined />
                            &nbsp;&nbsp;
                            <Badge dot color="rgb(45, 183, 245)">
                                <FileTextOutlined/>
                            </Badge>
                        </div>
                    </div>
                </div>
            </header>
            
            <div style={{padding:'5px 20px',display:'flex'}}>
                <div>
                    <UnorderedListOutlined style={{fontSize: '18px',
                        marginTop: '18px',
                        paddingBottom: '8px',
                        paddingRight: '18px',
                        borderBottom: '1px solid #e8e1e1'}} 
                        onClick={() => setOpen(true)}
                    />
                </div>
                {/* <Tabs defaultActiveKey="2" items={items} onChange={onChange} style={{overflowX:'auto'}}/> */}
                <Tabs defaultActiveKey="2" style={{overflowX:'auto'}}>
                    {items.map(item => (
                        <TabPane key={item.key} tab={item.label}>
                            {item.children}
                        </TabPane>
                    ))}
                </Tabs>
            </div>

            <FooterPage>
                <div style={{padding: '0 10px'}}>
                    <Button type="primary" style={{width: '100%'}} onClick={() => navigate("/food-bill")}>
                        ดำเนินการต่อ
                    </Button>
                </div>
            </FooterPage>

            <Drawer
                title={drawerHeader}
                placement="bottom"
                closable={false}
                onClose={onClose}
                open={open}
                key="bottom"
                className="all-categories"
            >
                <Row>
                    <Col span={24} style={{display:'flex',justifyContent:'space-between'}}>
                        <div> เมนูแนะนำ </div>
                        <div>
                            <CheckOutlined style={{color:'#4096ff'}} />
                        </div>
                    </Col>
                    <Col span={24} style={{display:'flex',justifyContent:'space-between'}}>
                        <div> กะเพรา </div>
                        <div>
                            <CheckOutlined style={{color:'#4096ff'}} />
                        </div>
                    </Col>
                    <Col span={24} style={{display:'flex',justifyContent:'space-between'}}>
                        <div> ต้มยำ </div>
                        <div>
                            <CheckOutlined style={{color:'#4096ff'}} />
                        </div>
                    </Col>
                    <Col span={24} style={{display:'flex',justifyContent:'space-between'}}>
                        <div> เมนูเส้น </div>
                        <div>
                            <CheckOutlined style={{color:'#4096ff'}} />
                        </div>
                    </Col>
                    <Col span={24} style={{display:'flex',justifyContent:'space-between'}}>
                        <div> จำนวนลูกค้า </div>
                        <div>
                            <CheckOutlined style={{color:'#4096ff'}} />
                        </div>
                    </Col>
                </Row>
            </Drawer>
        </div>
    );
}

export default MenuListView;