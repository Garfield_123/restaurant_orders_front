import React from "react";
import { useNavigate } from "react-router-dom";
import { Button, Tag, Card, Radio } from 'antd';
import { LeftOutlined, InfoCircleOutlined, UserOutlined } from '@ant-design/icons';
import HeaderPage from "../../components/page/header";
import FooterPage from "../../components/page/footer";

const FoodBillView = () => {
    const navigate = useNavigate();

    return (
        <div>
            <HeaderPage>
                <div style={{marginLeft: '20px'}}>
                    <LeftOutlined onClick={() => navigate("/history-order")}/>
                </div>
                <div style={{textAlign:'center'}}>
                    <div>บิลค่าอาหาร</div>
                </div>
            </HeaderPage>
            
            <div>
                <div style={{display:'flex',justifyContent: 'space-between', padding:'20px 20px 10px 20px'}}>
                    <div>
                        <div> รายการที่สั่ง (10) </div>
                    </div>
                    <div>
                        <Tag color="blue" style={{marginRight: 0}}>โต๊ะ 15</Tag>
                    </div>
                </div>
                <hr style={{width: '90%'}}/>
            </div>
            <div>
                <div style={{display:'flex',justifyContent: 'space-between', padding:'20px 20px 10px 20px'}}>
                    <div style={{display:'flex'}}>
                        <div> 1x </div>
                        <div style={{paddingLeft: '20px'}}>
                            <div> 3 คน </div>
                            <div style={{marginTop:'10px'}}> สั่งโดย: A </div>
                        </div>
                    </div>
                    <div> ฿ 0 </div>
                </div>
                <hr style={{width: '90%'}}/>
            </div>
            <div>
                <div style={{display:'flex',justifyContent: 'space-between', padding:'20px 20px 10px 20px'}}>
                    <div style={{display:'flex'}}>
                        <div> 2x </div>
                        <div style={{paddingLeft: '20px'}}>
                            <div> น้ำเปล่า </div>
                            <div style={{marginTop:'10px'}}> สั่งโดย: A </div>
                        </div>
                    </div>
                    <div> ฿ 20 </div>
                </div>
                <hr style={{width: '90%'}}/>
            </div>
            <div style={{padding:'20px'}}>
                <div style={{display:'flex',justifyContent:'space-between'}}>
                    <div> รวม </div>
                    <div> ฿420.00 </div>
                </div>
                <div style={{display:'flex',justifyContent:'space-between'}}>
                    <div> ราคารวม (รวมภาษี) </div>
                    <div> ฿420.00 </div>
                </div>
            </div>

            <div style={{padding:'10px'}}>
                <Card style={{boxShadow: '4px 2px 2px 0 rgba(0, 0, 0, 0.03)'}}>
                    <div> วิธีการชำระเงิน </div>
                    <div style={{textAlign:'center'}}>
                        <Tag color="warning" style={{margin:'20px 0', padding:'10px'}}>
                            <InfoCircleOutlined/>
                            &nbsp;&nbsp;
                            ชำระเงินค่าอาหาร เมื่อคุณทานเสร็จเรียบร้อยแล้วเท่านั้น
                        </Tag>
                        <Card style={{boxShadow: '4px 2px 2px 0 rgba(0, 0, 0, 0.03)'}}>
                            <div style={{display:'flex',justifyContent:'space-between'}}>
                                <div>
                                    <UserOutlined/>
                                    &nbsp;&nbsp;
                                    ชำระเงินกับพนักงาน
                                </div>
                                <div>
                                    <Radio/>
                                </div>
                            </div>
                        </Card>
                    </div>
                </Card>
            </div>

            <FooterPage>
                <div style={{padding: '0 10px'}}>
                    <Button type="primary" style={{width: '100%'}} onClick={() => navigate("/food-bill")}>
                        ดำเนินการต่อ
                    </Button>
                </div>
            </FooterPage>
        </div>
    );
}

export default FoodBillView;