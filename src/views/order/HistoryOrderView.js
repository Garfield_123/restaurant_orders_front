import React from "react";
import { useNavigate } from "react-router-dom";
import { Tag, Button } from 'antd';
import { InfoCircleOutlined, FileTextOutlined, CloseOutlined } from '@ant-design/icons';
import HeaderPage from "../../components/page/header";
import FooterPage from "../../components/page/footer";

const HistoryOrderView = () => {
    const navigate = useNavigate();

    return (
        <div>
            <HeaderPage>
                <div style={{marginLeft: '20px'}}>
                    <CloseOutlined onClick={() => navigate("/")}/>
                </div>
                <div style={{textAlign:'center'}}>
                    <div>รายการที่สั่ง</div>
                    <div>ข้อมูล ณ เวลา 16:49</div>
                </div>
            </HeaderPage>
            <div>
                <div style={{padding:'20px 20px 10px 20px', display:'flex', alignItems:'center',justifyContent:'space-between'}}>
                    <div style={{display:'flex', alignItems:'center',justifyContent:'space-between'}}>
                        <div>
                            <div>
                                <img src={'../../images/logo.jpg'} style={{width: '100px', height: '90px',borderRadius: '12px',objectFit: 'cover'}}/>
                            </div>
                            <div style={{marginTop: '10px'}}> สั่งโดย: A </div>
                        </div>
                        <div style={{paddingLeft:'20px'}}>
                            <div style={{marginBottom:'20px'}}> 3 คน </div>
                            <div>  x 1 </div>
                        </div>
                    </div>
                    <div style={{textAlign:'right'}}>
                        <div style={{marginBottom:'20px'}}> ฿ 0 </div>
                        <div >
                            <Tag color="#ffa500" style={{marginRight: 0}}>กำลังทำ</Tag>
                        </div>
                    </div>
                </div>
                <hr style={{width: '90%'}}/>
            </div>
            <div>
                <div style={{padding:'20px 20px 10px 20px', display:'flex', alignItems:'center',justifyContent:'space-between'}}>
                    <div style={{display:'flex', alignItems:'center',justifyContent:'space-between'}}>
                        <div>
                            <div>
                                <img src={'../../images/logo.jpg'} style={{width: '100px', height: '90px',borderRadius: '12px',objectFit: 'cover'}}/>
                            </div>
                            <div style={{marginTop: '10px'}}> สั่งโดย: A </div>
                        </div>
                        <div style={{paddingLeft:'20px'}}>
                            <div style={{marginBottom:'20px'}}> น้ำเปล่า </div>
                            <div>  X 2 </div>
                        </div>
                    </div>
                    <div style={{textAlign:'right'}}>
                        <div style={{marginBottom:'20px'}}> ฿ 20 </div>
                        <div >
                            <Tag color="#ffa500" style={{marginRight: 0}}>กำลังทำ</Tag>
                        </div>
                    </div>
                </div> 
                <hr style={{width: '90%'}}/>
            </div>
            <FooterPage>
                <div style={{display:'flex', alignItems:'center', justifyContent:'space-between',margin:'20px 0',padding: '0 15px'}}>
                    <div> 9 รายการ </div>
                    <div> 
                        ฿340.00 
                        &nbsp;&nbsp;
                        <InfoCircleOutlined style={{color:'#4096ff'}}/>
                    </div>
                </div>
                <div style={{marginBottom: '20px',padding: '0 10px'}}>
                    <Button type="primary" ghost style={{width: '100%'}} onClick={() => navigate("/food-bill")}>
                        <FileTextOutlined style={{color:'#4096ff'}}/>
                        บิลค่าอาหาร
                    </Button>
                </div>
            </FooterPage>
        </div>
    );
}

export default HistoryOrderView;