import React from 'react';
// import { withRouter } from "react-router-dom";
import Routes from "../../router";
import { Layout } from 'antd';
const { Content }  = Layout;
const LayoutFull = () => {
  
  return (
    <Layout className="layout">
        <Content className='box-content'>
            <Routes />
        </Content>
    </Layout>
  );
};
export default LayoutFull;